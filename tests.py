import pandas as pd
import pytest
from more_itertools.more import always_iterable
from selenium.webdriver.support.ui import WebDriverWait

from constants import urls
from constants import users
from constants.input_variables import *
from constants.loggers import AUTH_LOGGER
from functions import Functions


class TestClass:
    def setup_class(self):
        global driver
        global wait
        for driver in drivers:
            wait = WebDriverWait(driver, DEFAULT_LOAD_TIME)
            driver.set_page_load_timeout(120)  # set to 2 minutes instead of 5
            self.f = Functions(driver, wait)
            AUTH_LOGGER.info('running on ' + driver.capabilities['browserName'])
            # TODO: user isn't needed to log out, want to set authentication to false, but ugh
            self.f.logoutBeforeStartingTests(users.ADMIN_TEST_USER, logout_by_clicking=False)

    def test_allCompanyDashboardsLoadAsCompanyUser(self):
        # TODO: move assert out of here and into test
        for user in users.MOCK:
            old_url = driver.current_url
            user.login(driver, urls.BASE.name, use_login_button=False)
            self.f.homepageIsLoaded(old_url)
            self.f.logout(user, logout_by_clicking=True)

    def test_allCompanyDashboardsLoadAsR3moteUser(self):
        # TODO: move assert out of here and into test
        users.ADMIN_TEST_USER.login(driver, urls.BASE.name, use_login_button=False)
        self.f.goToCompanyDashboards(urls.COMPANY_DASHBOARD.name)
        self.f.goToEachCompanyDashboardAsR3MoteUser()
        self.f.logout(users.ADMIN_TEST_USER, logout_by_clicking=True)

    def test_404NotFoundPage(self):
        users.ADMIN_TEST_USER.login(driver, urls.BASE.name, use_login_button=False)
        driver.get(urls.NON_EXISTANT.name)
        assert not self.f.pageIsNot404()
        self.f.logout(users.ADMIN_TEST_USER, logout_by_clicking=False)

    @pytest.mark.miscellaneous
    def test_logout(self):
        users.ADMIN_TEST_USER.login(driver, urls.BASE.name, use_login_button=True)
        self.f.logout(users.ADMIN_TEST_USER, logout_by_clicking=True)

    @pytest.mark.miscellaneous
    def test_login(self):
        # Try logging in and out using all combination of buttons
        users.ADMIN_TEST_USER.login(driver, urls.BASE.name, use_login_button=True)
        self.f.logout(users.ADMIN_TEST_USER, logout_by_clicking=True)
        users.ADMIN_TEST_USER.login(driver, urls.BASE.name, use_login_button=False)
        self.f.logout(users.ADMIN_TEST_USER, logout_by_clicking=True)
        users.ADMIN_TEST_USER.login(driver, urls.BASE.name, use_login_button=True)
        self.f.logout(users.ADMIN_TEST_USER, logout_by_clicking=False)
        users.ADMIN_TEST_USER.login(driver, urls.BASE.name, use_login_button=False)
        self.f.logout(users.ADMIN_TEST_USER, logout_by_clicking=False)

    def test_authentication(self):
        # TODO: add in test_logout and test_login to here
        AUTH_LOGGER.error('==================== authentication test - start ====================')
        # TODO: make a less hacky solution
        table_filepath = 'misc/user_permissions_table.xlsx'
        df = pd.read_excel(table_filepath)
        # TODO: need to remove hardcoding, pass in to function
        users_to_test = users.UNIQUE_DASHBOARDS

        def go_to_each_auth_url(user):
            AUTH_LOGGER.info('\n')
            AUTH_LOGGER.info(f'user permissions, {user.permissions}')
            for url in urls.Url.instances:
                AUTH_LOGGER.debug('\n')
                AUTH_LOGGER.info(f'url, {url.name}')
                AUTH_LOGGER.debug(f'url permissions, {url.permissions}')
                AUTH_LOGGER.debug(f'user permissions, {user.permissions}')
                if (user.has_url_access(url)) and (url.name not in urls.REDIRECT_URLS):
                    driver.get(url.name)
                    was_successful = self.f.went_to_url(url.name)
                else:
                    driver.get(url.name)
                    was_successful = self.f.went_to_url(urls.REDIRECT_URLS)
                if not was_successful:
                    AUTH_LOGGER.error('\n')
                    AUTH_LOGGER.error(f'user, {user.name}')
                    AUTH_LOGGER.error(f'url, {url.name}')
                    AUTH_LOGGER.error(f'url permissions, {url.permissions}')
                    AUTH_LOGGER.error(f'user permissions, {user.permissions}')
                    AUTH_LOGGER.error(f'url final, {driver.current_url}')
                AUTH_LOGGER.debug(f'url final, {driver.current_url}')

        for user in always_iterable(users_to_test):
            AUTH_LOGGER.info(f'user, {user.name}')
            user.login(driver, urls.BASE.name, use_login_button=True)
            go_to_each_auth_url(user)
            # For the list of urls, check that the user has permissions for it
            self.f.logout(user, logout_by_clicking=True)

        def check_urls_while_not_authenticated(user):
            AUTH_LOGGER.info('check urls when user is not authenticated')
            AUTH_LOGGER.info(f'user, {user.name}')
            self.f.logoutBeforeStartingTests(user, logout_by_clicking=False)
            user.set_details(df)
            go_to_each_auth_url(user)

        check_urls_while_not_authenticated(users.ADMIN_TEST_USER)
        AUTH_LOGGER.error('==================== authentication test - end ====================')
