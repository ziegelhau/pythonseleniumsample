from more_itertools.more import always_iterable
from selenium.common.exceptions import NoSuchWindowException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from constants import urls
from constants.input_variables import *
# from selenium.webdriver.common.keys import Keys
from constants.loggers import AUTH_LOGGER
from constants.misc import (
    LOADER_CLASS, )
# NOT_AUTHENTICATED_SIDEBAR_TEXT)

class Functions:
    def __init__(self, driver, wait):
        self.driver = driver
        self.wait = wait

    def logout(self, user, logout_by_clicking=False):
        # Click dropdown menu
        if logout_by_clicking:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@class='bm-burger-button']"))).click()
            # TODO: change all find_element to find_elements and if more than 1 throw error
            # Improve by finding the specific element and not searching through all
            AUTH_LOGGER.info(self.driver.find_element_by_class_name('bm-item-list').text)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[contains(text(), 'Logout')]"))).click()
            was_successful = self.went_to_url(urls.REDIRECT_URLS)
        else:
            self.driver.get(urls.LOGOUT.name)
            was_successful = self.went_to_url(urls.REDIRECT_URLS)
        user.permissions.authenticated = False

    def goToCompanyDashboards(self, url):
        self.driver.get(url)
        self.wait.until(lambda driver: driver.current_url == urls.COMPANY_DASHBOARD.name)
        # TODO: move assert out of here and into test
        assert self.driver.current_url == urls.COMPANY_DASHBOARD.name

    def goToEachCompanyDashboardAsR3MoteUser(self):
        # TODO: move assert out of here and into test
        # Am getting the links in each companyDashboard box instead of clicking each button and going back to the company dashboard page
        panels = self.wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'panel')))
        # TODO: figure out how to use and operator to directly get links inside of panels to remove for loop here
        company_links = [panel.find_element_by_xpath(".//a[@href]").get_attribute("href") for panel in panels]
        old_url = ''
        for link in company_links:
            AUTH_LOGGER.info('navigating to ' + link)
            self.driver.get(link)
            self.homepageIsLoaded(old_url)
            old_url = link

    def pageIsLoaded(self, old_url):
        self.wait.until(lambda driver: driver.current_url != old_url)
        self.wait.until(lambda driver: driver.current_url != urls.LOADING_PAGE.name)
        AUTH_LOGGER.info('homepage is ' + driver.current_url)
        # TODO: not so useful because footer seems to exist immediately
        footer_text = self.wait.until(lambda driver: driver.find_element_by_xpath("//div[@class='footer']")).text
        assert 'Powered by R3mote.io' in footer_text
        self.pageIsNot404()
        # If there's loaders wait for them to finish
        loaders = self.driver.find_elements_by_class_name(LOADER_CLASS)
        for loader in loaders:
            # TODO: once speed has improved change back to below line
            self.wait.until(EC.invisibility_of_element(loader))

    def homepageIsLoaded(self, old_url):
        """
        Home page is loaded if
            Not on the old url
            Not on the loading page
            footer with correct text exists
            At least 1 image exists on the page
            Page is not the 404 page
        """
        # TODO: Currently is generic, in the future will make specific for each unique url
        #    Would be even better to check for each unique user that is logging in
        #    Find out why the loaders don't show up every time
        self.wait.until(lambda driver: driver.current_url != old_url)
        self.wait.until(lambda driver: driver.current_url != urls.LOADING_PAGE.name)
        AUTH_LOGGER.info('homepage is ' + self.driver.current_url)
        self.wait.until(lambda driver: driver.find_elements_by_tag_name('img'))
        footer_text = self.wait.until(lambda driver: driver.find_element_by_xpath("//div[@class='footer']")).text
        self.pageIsNot404()
        assert 'Powered by R3mote.io' in footer_text
        # Get loaders afterwards because footer will be loaded if anything gets done
        loaders = self.driver.find_elements_by_class_name(LOADER_CLASS)
        # If there's loaders wait for them to finish
        for loader in loaders:
            # TODO: once speed has improved change back to below line
            # self.wait.until(EC.invisibility_of_element(loader))
            WebDriverWait(self.driver, 60).until(EC.invisibility_of_element(loader))

    def logoutBeforeStartingTests(self, user, logout_by_clicking=False):
        # Edge will cache credentials so may need to logout if that has not been done
        while True:
            try:
                self.wait.until(lambda driver: driver.page_source == '<html><head></head><body></body></html>')
                break
            # NoSuchWindowException gets thrown if driver.page_source does not exist yet (edge ugh)
            except NoSuchWindowException:
                pass
        self.driver.get(urls.BASE.name)
        if self.driver.current_url != urls.BASE.name:
            self.logout(user, logout_by_clicking)

    def pageIsNot404(self):
        # Returns False no our 404 page
        return '404\nPage Not Found\nGO BACK\nPowered by R3mote.io' not in self.wait.until(
            EC.visibility_of_element_located((By.ID, 'root'))).text

    # TODO: put function in Url class
    def went_to_url(self, urls):
        try:
            # Returns true if completed
            return self.wait.until(lambda driver: driver.current_url in always_iterable(urls))
        except TimeoutException:
            return False
