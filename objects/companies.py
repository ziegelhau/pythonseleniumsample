# UNFINISHED

class CompanyTemplate:
    homepage = 'dashboard'
    name = None
    sidebar_text = '\n'.join([
        'Home',
        'Settings',
        'Leave Feedback New',
        'Support',
        'Logout'])
    urls_not_allowed = None


class Company1(CompanyTemplate):
    homepage = 'leak-detection'
    sidebar_text = 'Leak Detection\nSettings\nLeave Feedback New\nSupport\nLogout'


class Company2(CompanyTemplate):
    homepage = 'company2'
    sidebar_text = '\n'.join([
        'Home',
        'Reports',
        'Settings',
        'Leave Feedback New',
        'Support',
        'Logout'])
    urls_not_allowed = ['dashboard']


class Company3(CompanyTemplate):
    sidebar_text = '\n'.join([
        'Home',
        'Reporting Download',
        'Pipeline Data Download',
        'Status',
        'Route Planning',
        'Company Dashboards',
        'Reports Demo',
        'Admin Pages',
        'User Management',
        'Reports',
        'Settings',
        'Leave Feedback',
        'Support',
        'Logout'])
