from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from constants import urls
from constants.loggers import AUTH_LOGGER
from objects.permissions import Permissions


class User:
    # TODO: make mixin
    instances = []
    dont_track = []

    def __init__(self, name, password):
        if name not in User.dont_track:
            User.instances.append(self)
        self.name = name
        self.password = password
        self.details = {}
        self.permissions = Permissions()
        self.company = None

    def has_url_access(self, url):
        permissions = url.permissions
        # Check if the user has the correct permissions to access the url
        if not isinstance(permissions, Permissions):
            AUTH_LOGGER.info('has permission, no')
            return False
        # TODO: If it's company specific return True for now because we don't have that coded
        if url.allowed_companies is not None:
            AUTH_LOGGER.info('has permission, yes')
            return True
        for key, value in vars(permissions).items():
            if value is not None:
                # Authenticated is a special case
                # If the user is authenticated but the url does not require authorization, it is still valid
                if key == 'authenticated':
                    # Only return false if url requires authentication and user does not have it
                    if (not self.permissions.authenticated) and value:
                        AUTH_LOGGER.info('has permission, no')
                        return False
                else:
                    if getattr(self.permissions, key) != value:
                        AUTH_LOGGER.info('has permission, no')
                        return False
        AUTH_LOGGER.info('has permission, yes')
        return True

    # Get the users permissions and stuff
    def set_details(self, permissions_df):
        self.details = permissions_df.loc[permissions_df['username'] == self.name].to_dict('records')[0]
        self.permissions.is_client = self.details['is_client']
        self.permissions.paidUser = self.details['paidUser']
        self.permissions.is_staff = self.details['is_staff']

    def login(self, driver, url, use_login_button):
        AUTH_LOGGER.info('logging in as ' + self.name)
        driver.get(url)  # waits until url is loaded by default
        wait = WebDriverWait(driver, 8)  # create specific instance of webdriverwait with shorter timeout
        username_box = wait.until(EC.visibility_of_element_located((By.ID, "username")))
        username_box.clear()
        username_box.send_keys(self.name)
        password_box = wait.until(EC.visibility_of_element_located((By.ID, "password")))
        password_box.clear()
        password_box.send_keys(self.password)
        if use_login_button:
            wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'r3mote-button'))).click()
        else:
            password_box.send_keys(Keys.ENTER)
        wait.until(lambda driver: driver.current_url == urls.LOADING_PAGE.name)
        # Having some loading too fast issues, crashing when trying to navigate to company dashboards
        # Should be some intermediate test I think, wait for loading page to finish or something
        # Lets local storage stuff be set, in reality, client doesn't navigate away from loading page immediately
        wait.until(lambda driver: driver.current_url != urls.LOADING_PAGE.name)
        self.permissions.authenticated = True

    def __str__(self):
        return self.name
