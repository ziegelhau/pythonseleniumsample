# TODO: may want to use a  creational design pattern to incrementally build the permissions
# TODO: So we don't have to set them all to none immediately
class Permissions:
    def __init__(self, is_client=None, is_staff=None, authenticated=True):
        self.is_client = is_client
        self.is_staff = is_staff
        self.authenticated = authenticated

    def __str__(self):
        # Return only key and values that are not none
        return str({k: v for k, v in vars(self).items() if v is not None})
