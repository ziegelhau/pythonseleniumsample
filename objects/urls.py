from more_itertools.more import always_iterable

from constants.input_variables import URL_BASE
from objects.permissions import Permissions


class Url:
    instances = []
    dont_track = []

    def __init__(self, name, permissions=Permissions(), companies=None, redirect_url=None):
        if name not in Url.dont_track:
            Url.instances.append(self)
        self.name = URL_BASE + name
        self.permissions = permissions
        self.allowed_companies = always_iterable(companies)
        self.redirect_url = redirect_url

    def __str__(self):
        return self.name
