import logging
AUTH_LOGGER = logging.getLogger('authentication')
AUTH_LOGGER.setLevel(logging.DEBUG)
# create a file handler
error_handler = logging.FileHandler('misc/logs/authentication_test/errors.log')
error_handler.setLevel(logging.ERROR)
# add the file handler to the logger
AUTH_LOGGER.addHandler(error_handler)
info_handler = logging.FileHandler('misc/logs/authentication_test/info.log')
info_handler.setLevel(logging.INFO)
# add the file handler to the logger
AUTH_LOGGER.addHandler(info_handler)
# CRITICAL: 50
# ERROR: 40
# WARNING:30
# INFO:20
# DEBUG:10
# NOTSET:0