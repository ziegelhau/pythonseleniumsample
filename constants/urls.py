from objects.permissions import Permissions
from objects.urls import Url

is_staff = Permissions(is_staff=True, authenticated=True)
not_authenticated = Permissions(authenticated=False)

logout = 'logout'
loading_page = 'loading.html'
login_page = 'login'
Url.dont_track = [logout, loading_page, login_page]

BASE = Url('', not_authenticated)
LOGIN_PAGE = Url(login_page, not_authenticated)
LOADING_PAGE = Url(loading_page)
USER_MANAGEMENT = Url('user-management', is_staff, redirect_url=LOADING_PAGE.name)
COMPANY_DASHBOARD = Url('company-dashboards',
                        is_staff)  # can load too fast and crash, know the chang has been fixed though
LOGOUT = Url('logout')
CLIENT_SAFE_DEVICE = Url('client-safe-device', is_staff)
NON_EXISTANT = Url('aaaaaaaaa')
# - Navigate to these directly and through buttons as each user type, see that they succeed and fail
DASHBOARD = Url('dashboard')
PIPELINE_DATA = Url('pipeline-data')
AUDIT_SUMMARY = Url('audit-summary')
CALIBRATION_FORM = Url('calibration-form')
STATUS = Url('status')
LEAK_DETECTION = Url('leak-detection')
PIPELINE_DASHBOARD = Url('pipeline-dashboard')
ROUTE_PLANNING = Url('route-planning')
COMPANY_DASHBOARDS = Url('company-dashboards')
CLIENT_SAFE_DEVICE = Url('client-safe-device')
TRUCK_LOADOUT = Url('truck-loadout')
REPORTING_DEMO = Url('reporting-demo')
HVAC_DEMO = Url('hvac-demo')
USER_MANAGEMENT = Url('user-management')
UPDATE_PASSWORD = Url('update-password')
SETTINGS = Url('settings')
DEMO = Url('demo')
DEMO_DEMO_DASHBOARDS = Url('demo/demo-dashboards')
DEMO_DASHBOARD = Url('demo/dashboard')
DEMO_TEMPERATURE_DASHBOARD = Url('demo/temperature-dashboard')
DEMO_MAP_DASHBOARD = Url('demo/map-dashboard')
RELEASENOTES = Url('releasenotes')
FEEDBACK = Url('feedback')

COMPANY3_SIDEBAR = [
    DASHBOARD,
    'Reporting Download',
    'Pipeline Data Download',
    'Status',
    'Route Planning',
    'Company Dashboards',
    'Reports Demo',
    'Admin Pages',
    'User Management',
    'Reports',
    'Settings',
    'Leave Feedback',
    'Support',
    'Logout']

company_dashboards = ['company1_dashboard',
                      'company2_dashboard',
                      'company3_dashboard']
REDIRECT_URLS = (BASE.name, LOGIN_PAGE.name, LOADING_PAGE.name)
