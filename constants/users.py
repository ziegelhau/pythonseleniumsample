import logging

logger = logging.getLogger(__name__)
from objects.users import User

COMPANY1_TEST_USER = User('company1_test_user@r3mote.io', 'randomly_generated_password1')
COMPANY2_TEST_USER = User('company2_test_user@r3mote.io', 'randomly_generated_password2')
ADMIN_TEST_USER = User("admin_test_user@r3mote.io", "randomly_generated_password3")
NOT_A_USER = User("aaa", "aaa")

# TODO: mock users are
MOCK = [
    COMPANY1_TEST_USER,
    COMPANY2_TEST_USER
]

UNIQUE_DASHBOARDS = [
    COMPANY1_TEST_USER
]
