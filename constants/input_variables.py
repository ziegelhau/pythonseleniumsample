from selenium import webdriver

# Initialize variables
PATH_TO_CHROMEDRIVER = './drivers/chromedriver_win32/chromedriver.exe'
wait = None
drivers = []
driver = None
# ----------INPUT VARIABLES---------- #
DEFAULT_LOAD_TIME = 20  # Time to wait for page/element to load before throwing error
URL_BASE = 'http://localhost:3000/'
# URL_BASE = 'https://our_company_url.com/'
chrome = webdriver.Chrome(PATH_TO_CHROMEDRIVER)
drivers.append(chrome)
# edge = webdriver.Edge()
# drivers.append(edge)
# ----------INPUT VARIABLES---------- #
