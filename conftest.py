from kill_drivers import kill_drivers


# Use this to run code before any tests start
def pytest_configure():
    kill_drivers()
